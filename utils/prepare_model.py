# -*- coding: utf-8 -*-
"""Prepare the zip file for the model
"""
from refutils.utils import ReferenceModel


class OC4Jacket(ReferenceModel):
    def __init__(self):
        model_kwargs = dict(cut_in=4, cut_out=25, dt=40, tstart=100, wsp=24,
                            tb_wid=130, tb_ht=130) # rotor diameter is 126 m
        model_kwargs['tint'] = 0.14*(0.75*model_kwargs['wsp'] + 5.6) / model_kwargs['wsp']  # class 1B
        dll_list = [('DISCON_NREL5MW_v0.2.0.dll', 'DISCON.dll'),
                    ('hss_convert_5mw.dll', 'hss_convert.dll'),
                    ('nrel_5mw_interface_v0.2.0.dll', 'nrel_5mw_interface.dll')]
        ReferenceModel.__init__(self,
                                model_path_name='oc4-jacket',
                                htc_basename='oc4_jacket_phase_1',
                                model_kwargs=model_kwargs,
                                dll_list=dll_list,
                                step=True, turb=True, hs2=False)


if __name__ == '__main__':
    model = OC4Jacket()
    model.prepare_model()
    #model.test_with_hawc2binary_master()
